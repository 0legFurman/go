package main

import (
	"strconv"
)

type Bank struct {
	Name      string
	bankMoney float32
	Deposit   float32
	Credit    float32
	Clients   []Client
}

func (bank *Bank) SetName(name string) {
	bank.Name = name
}

func (bank *Bank) SetBankMoney(bankMoney float32) {
	bank.bankMoney = bankMoney
}

func (bank *Bank) SetDeposit(Deposit float32) {
	bank.Deposit = Deposit
}

func (bank *Bank) SetCredit(Credit float32) {
	bank.Credit = Credit
}

func (bank *Bank) AddClient(client Client) {
	go client.depositCheck()
	bank.Clients = append(bank.Clients, client)
}

func (bank Bank) getName() string {
	return bank.Name
}

func (bank Bank) findById(number int) string {
	var result Client
	for i := 0; i < len(bank.Clients); i++ {
		if bank.Clients[i].AccountNumber == number {
			result = bank.Clients[i]
		}
	}
	return result.getNameSurname()
}

func (bank *Bank) generateUsers(col int) {
	for i := 1; i < col; i++ {
		client := NewClient()
		client.SetName("Test" + strconv.Itoa(i))
		client.SetSurname("Test" + strconv.Itoa(i))
		client.GenerateNumber()
		client.SetCredit(3.33)
		client.SetDeposit(4.44)
		bank.Clients = append(bank.Clients, *client)
	}
}
