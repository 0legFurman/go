package main

import (
	"fmt"
	"strconv"
)

func printClients(clients []Client) {

	for i := 0; i < len(clients); i++ {

		fmt.Println("\n" + clients[i].getNameSurname())
		fmt.Println(clients[i].AccountNumber)
	}
}

func main() {
	var bank Bank
	bank.SetName("Mono")
	fmt.Print(bank.getName())

	bank.generateUsers(4)

	printClients(bank.Clients)
	accNum := int(bank.Clients[2].AccountNumber)
	print("\nFind by id: " + strconv.Itoa(accNum))
	print("\n" + bank.findById(accNum))

	bank.Clients[2].SetDeposit(0.0005)

}
