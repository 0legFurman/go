package main

import (
	"math/rand"
	"strconv"
	"time"
)

type Client struct {
	Name          string
	Surname       string
	AccountNumber int
	cDeposit      chan float32
	cCredit       float32
}

func NewClient() *Client {
	cDeposit := make(chan float32)
	return &Client{cDeposit: cDeposit}
}

func (client *Client) SetName(name string) {
	client.Name = name
}

func (client *Client) SetSurname(surname string) {
	client.Surname = surname
}

func (client Client) getNameSurname() string {
	return client.Name + " " + client.Surname
}

func (client *Client) GenerateNumber() {
	rand.Seed(time.Now().UnixNano())
	client.AccountNumber = int(rand.Int31())
}

func (client *Client) depositCheck() {
	lastDeposit := <-client.cDeposit
	if lastDeposit != 0 && lastDeposit < float32(0.009) {
		print("\nUser " + strconv.Itoa(client.AccountNumber) + " complete credit limit")
		close(client.cDeposit)
	}
	time.Sleep(time.Second * 2)
}

func (client *Client) SetDeposit(deposit float32) {
	go client.depositCheck()
	client.cDeposit <- deposit
}

func (client *Client) SetCredit(credit float32) {
	client.cCredit = credit
}
