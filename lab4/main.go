package main

import (
	"fmt"
	"github.com/andlabs/ui"
	_ "github.com/andlabs/ui/winmanifest"
	"strconv"
)

func initGUI() {
	window := ui.NewWindow("Склопакет", 500, 150, false)

	box := ui.NewVerticalBox()
	box.SetPadded(true)

	box.Append(func() *ui.Box {
		box := ui.NewHorizontalBox()
		box.SetPadded(true)
		title := ui.NewLabel("Розміри вікна")
		box.Append(title, true)
		box.Append(ui.NewLabel(""), true)
		box.Append(ui.NewLabel("Склопакет"), true)

		return box
	}(), false)

	width := ui.NewEntry()
	windowType := ui.NewCombobox()

	box.Append(func() *ui.Box {
		box := ui.NewHorizontalBox()
		box.SetPadded(true)
		box.Append(ui.NewLabel("Ширина (см)"), true)
		box.Append(width, true)
		windowType.Append("Однокамерний")
		windowType.Append("Двокамерний")
		windowType.SetSelected(0)
		box.Append(windowType, true)

		return box
	}(), false)

	height := ui.NewEntry()

	box.Append(func() *ui.Box {
		box := ui.NewHorizontalBox()
		box.SetPadded(true)
		box.Append(ui.NewLabel("Висота (см)"), true)
		box.Append(height, true)
		box.Append(ui.NewLabel(""), true)

		return box
	}(), false)

	material := ui.NewCombobox()
	sill := ui.NewCheckbox("Підвіконня")

	box.Append(func() *ui.Box {
		box := ui.NewHorizontalBox()
		box.SetPadded(true)
		box.Append(ui.NewLabel("Матеріал"), true)
		material.Append("Дерево")
		material.Append("Метал")
		material.Append("Металопластик")
		material.SetSelected(0)
		box.Append(material, true)
		box.Append(sill, true)

		return box
	}(), false)

	priceLabel := ui.NewLabel("")
	submitButton := ui.NewButton("Розрахувати")

	box.Append(func() *ui.Box {
		box := ui.NewHorizontalBox()
		box.SetPadded(true)
		box.Append(ui.NewLabel("Вартість"), true)
		box.Append(priceLabel, true)
		box.Append(submitButton, true)

		return box
	}(), false)

	window.SetChild(box)

	submitButton.OnClicked(func(button *ui.Button) {
		widthValue, _ := strconv.ParseFloat(width.Text(), 32)
		heightValue, _ := strconv.ParseFloat(height.Text(), 32)
		materialValue := material.Selected()
		var price float64
		switch materialValue {
		case 0:
			price = 0.25
		case 1:
			price = 0.05
		case 2:
			price = 0.15
		}
		windowTypeValue := windowType.Selected()
		switch windowTypeValue {
		case 0:
		case 1:
			price += 0.05
		}
		totalPrice := widthValue * heightValue * price
		if sill.Checked() {
			totalPrice += 35
		}

		priceLabel.SetText(fmt.Sprintf("%f", totalPrice))
	})
	window.OnClosing(func(window *ui.Window) bool {
		ui.Quit()
		return true
	})
	window.Show()
}

func main() {
	err := ui.Main(initGUI)
	if err != nil {
		panic(err)
	}
}
