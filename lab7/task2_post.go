package main

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

const (
	pageHeader = `<!DOCTYPE HTML>
<html>
<head>
	<title>Задание 2</title>
	<style>
	.error{
	color:#FF0000;
	}         
	</style>
</head>`
	pageBody = `<body>
<h1>Решение</h1>
<p></p>`
	form = `<form action="/" method="POST">
	<label for="array">Введите массив значений через пробел</label><br />
	<input type="text" name="array" size="100" required><br />
	<input type="submit" value="Выполнить">
</form>`
	pageFooter = `</body></html>`
	table      = `<table border="1"> 
	<tr><th colspan="2">Результат</th></tr>
	<tr><td>Строка 1</td><td>%s</td></tr>
	<tr><td>Строка 2</td><td>%f</td></tr>
</table>`
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание2"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form)
	if r.Method == "POST" {
		err := r.ParseForm()
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		parray := post.Get("array")
		stringArray := strings.Split(parray, " ")
		var intArray []int
		for _, s := range stringArray {
			num, err := strconv.Atoi(s)
			if err == nil {
				intArray = append(intArray, num)
			}
		}
		var x1 int = 1
		var x2 int = 0
		var firstElem int
		var lastElem int
		for index, value := range intArray {
			if index%2 == 0 {
				x1 *= value
			}
			if value == 0 {
				lastElem = index
			}
		}
		for index, value := range intArray {
			if value == 0 {
				firstElem = index
				break
			}
		}
		for i := firstElem; i < lastElem+1; i++ {
			x2 += intArray[i]
		}
		fmt.Fprintf(w, "Произведение элементов с четными номерами = %v<br>", x1)
		fmt.Fprintf(w, "Cумма элементов между первым и последним нулевыми элементами = %v", x2)
	}
	fmt.Fprint(w, "\n", pageFooter)
}
func main() {
	http.ListenAndServe("localhost:80", HttpSolution1)
}
