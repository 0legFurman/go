package main

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
)

const (
	pageHeader = `<!DOCTYPE HTML>
<html>
<head>
	<title>Задание 1</title>
	<style>
	.error{
	color:#FF0000;
	}         
	</style>
</head>`
	pageBody = `<body>
<h1>Решение</h1>
<p>ax<sup>2</sup> + bx + c = 0</p>`
	form = `<form action="/" method="POST">
	<label for="a">Введите число a:</label><br />
	<input type="text" name="a" size="20" value="1" required><br />
	<label for="b">Введите число b:</label><br />
	<input type="text" name="b" size="20" value="1" required><br />
	<label for="c">Введите число c:</label><br />
	<input type="text" name="c" size="20" value="1" required><br />
	<input type="submit" value="Выполнить">
</form>`
	pageFooter = `</body></html>`
	table      = `<table border="1"> 
	<tr><th colspan="2">Результат</th></tr>
	<tr><td>Строка 1</td><td>%s</td></tr>
	<tr><td>Строка 2</td><td>%f</td></tr>
</table>`
	anError = `<p class="error">%s</p>`
)

type Solution string

var HttpSolution1 Solution = "Задание1"

func (s Solution) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, pageHeader, pageBody, form) //  Формируем  страницу  в  браузере
	if r.Method == "POST" {                   //  Обрабатываем  входные  данные
		err := r.ParseForm() //  Парсим  форму
		post := r.PostForm
		if err != nil {
			fmt.Fprintf(w, anError, err)
			return
		}
		pa := post.Get("a")
		pb := post.Get("b")
		pc := post.Get("c")
		a, _ := strconv.ParseInt(pa, 10, 32)
		b, _ := strconv.ParseInt(pb, 10, 32)
		c, _ := strconv.ParseInt(pc, 10, 32)
		dis := int64(math.Pow(float64(b), 2)) - 4*a*c
		if dis > 0 {
			x1 := (float64(-b) - math.Sqrt(float64(dis))) / float64(2*a)
			x2 := (float64(-b) + math.Sqrt(float64(dis))) / float64(2*a)
			fmt.Fprintf(w, "Дискриминант = %v<br>", dis)
			fmt.Fprintf(w, "x1 = %v<br>", x1)
			fmt.Fprintf(w, "x2 = %v", x2)
		} else if dis == 0 {
			x1 := -b / (2 * a)
			x2 := x1
			fmt.Fprintf(w, "Дискриминант = %v<br>", dis)
			fmt.Fprintf(w, "x1 = %v<br>", x1)
			fmt.Fprintf(w, "x2 = %v", x2)
		} else {
			fmt.Fprintf(w, "Дискриминант = %v<br>", dis)
			fmt.Fprintf(w, "Корней нет")
		}
	}
	fmt.Fprint(w, "\n", pageFooter)
}
func main() { //  Запускаем  локальный  сервер
	http.ListenAndServe("localhost:80", HttpSolution1)
}
