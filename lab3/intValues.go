package main

import (
	"fmt"
	"math"
	"time"
)

func main() {
	const length int = 4000
	var randomNum [length]int64
	var randomNumConvert [length]int
	var a int64 = 2147483629
	var c int64 = 2147483587
	var max int64 = math.MinInt64
	var min int64 = math.MaxInt64
	m := math.Pow(2, 31) - 1
	randomNum[0] = time.Now().Unix()
	for i := 1; i < length; i++ {
		randomNum[i] = (a*randomNum[i-1] + c) % int64(m)
	}
	for i := 0; i < length; i++ {
		if randomNum[i] > max {
			max = randomNum[i]
		}
		if randomNum[i] < min {
			min = randomNum[i]
		}
	}
	for i := 0; i < length; i++ {
		randomNumConvert[i] = Convert(float64(randomNum[i]), float64(min), float64(max), 0, 150)
		fmt.Println(randomNumConvert[i])
	}

	repeat := 0
	var avg float64 = 0
	var chance [151]float64
	for i := 0; i < 151; i++ {
		for j := 0; j < length; j++ {
			if i == randomNumConvert[j] {
				repeat++
			}
		}
		chance[i] = float64(repeat) / float64(length)
		fmt.Println(i, "repeat ", repeat, " times")
		fmt.Println("Chance: ", chance[i])
		avg += chance[i] * float64(i)
		repeat = 0
	}
	fmt.Println("Average value: ", avg)

	var disp, dev float64
	for i := 0; i < 151; i++ {
		disp += math.Pow((float64(randomNumConvert[i])-avg), 2) * chance[i]
	}
	dev = math.Sqrt(disp)
	fmt.Println("Dispersion: ", disp, " Standard deviation: ", dev)
}

func Convert(value float64, from1 float64, from2 float64, to1 float64, to2 float64) int {
	return int(math.Round((value-from1)/(from2-from1)*(to2-to1) + to1))
}
