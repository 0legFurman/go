package main

import (
	"fmt"
	"math"
	"time"
)

func main() {
	const length int = 4000
	var randomNum [length]int64
	var randomNumConvert [length]float64
	var a int64 = 2147483629
	var c int64 = 2147483587
	var max int64 = math.MinInt64
	var min int64 = math.MaxInt64
	m := math.Pow(2, 31) - 1
	randomNum[0] = time.Now().Unix()
	for i := 1; i < length; i++ {
		randomNum[i] = (a*randomNum[i-1] + c) % int64(m)
	}
	for i := 0; i < length; i++ {
		if randomNum[i] > max {
			max = randomNum[i]
		}
		if randomNum[i] < min {
			min = randomNum[i]
		}
	}
	for i := 0; i < length; i++ {
		randomNumConvert[i] = ConvertFloat(float64(randomNum[i]), float64(min), float64(max), 0, 150)
		fmt.Println(randomNumConvert[i])
	}
}

func ConvertFloat(value float64, from1 float64, from2 float64, to1 float64, to2 float64) float64 {
	return (value-from1)/(from2-from1)*(to2-to1) + to1
}
