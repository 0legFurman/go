package main

import (
	"fmt"
)

func ReadProductsArray() [10]Product {
	condition := true
	var products [10]Product
	var i int
	for {
		var p Product
		fmt.Print("Name: ")
		fmt.Scanf("%s\n", &p.Name)
		fmt.Print("Price: ")
		fmt.Scanf("%d\n", &p.Price)
		fmt.Print("Name currency: ")
		fmt.Scanf("%s\n", &p.Cost.Name)
		fmt.Print("ExRate: ")
		fmt.Scanf("%d\n", &p.Cost.ExRate)
		fmt.Print("Quantity: ")
		fmt.Scanf("%d\n", &p.Quantity)
		fmt.Print("Producer: ")
		fmt.Scanf("%s\n", &p.Producer)
		fmt.Print("Weight: ")
		fmt.Scanf("%d\n", &p.Weight)
		fmt.Print("Continue? (1, 0) ")
		fmt.Scanf("%t\n", &condition)
		products[i] = p
		i++
		if !condition {
			break
		}
	}
	return products
}

func PrintProducts(product [10]Product) {
	for index := range product {
		fmt.Println("Name: ", product[index].Name)
	}
}

func main() {
	var products = ReadProductsArray()
	PrintProducts(products)
}
