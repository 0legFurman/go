package main

type Product struct {
	Name     string
	Price    int
	Cost     Currency
	Quantity int
	Producer string
	Weight   int
}

func (p Product) GetPriceIn() float32 {
	return float32(p.Price) * p.Cost.ExRate
}

func (p Product) GetTotalPrice() int {
	return p.Price * p.Quantity
}

func (p Product) GetTotalWeight() int {
	return p.Weight * p.Quantity
}
