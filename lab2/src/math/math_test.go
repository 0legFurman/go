package math

import (
	"testing"
)

func TestFDIF(t *testing.T) {
	x := fdif(1, 2)
	res := 3.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestFirstSeq(t *testing.T) {
	x := FirstSeq(1, 2, 3, 4, 5, 6, 7, 8)
	res := 3550.0
	if x != res {
		t.Errorf("Тест не пройден! Результат %f, а должен быть %f", x, res)
	}
}

func TestAriFmet(t *testing.T) {
	x := AriFmet([]int{1, 2, 3, 4, 5, 6, 7, 8})
	res := 4
	if x != res {
		t.Errorf("Тест не пройден! Результат %d, а должен быть %d", x, res)
	}
}

func TestArrayMin(t *testing.T) {
	x := ArrayMin([]int{1, 2, 3, 4, 5, 6, 7, 8})
	res := 1
	if x != res {
		t.Errorf("Тест не пройден! Результат %d, а должен быть %d", x, res)
	}
}
