package math

func AriFmet(array []int) int {
	var sum int = 0

	for i := 0; i < len(array); i++ {
		sum += array[i]
	}
	return sum / len(array)
}

func ArrayMin(array []int) int {
	var minValue int = 2147483647

	for i := 0; i < len(array); i++ {
		if minValue > array[i] {
			minValue = array[i]
		}
	}
	return minValue
}

func fdif(x1 float64, x2 float64) float64 {
	return x1 + x2
}

func FirstSeq(k1, k2, k3, k4, x0, y0, b, h float64) float64 {

	for x := x0; x <= b; x += h {
		k1 = fdif(x, y0)
		k2 = fdif(x+h/2, y0+h/2*k1)
		k3 = fdif(x+h/2, y0+h/2*k2)
		k4 = fdif(x+h, y0+h*k3)
		y0 += h * (k1 + 2*k2 + 2*k3 + k4) / 6
	}
	return y0
}
